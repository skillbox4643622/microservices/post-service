package ru.skillbox.postservice.service.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.skillbox.postservice.dto.PostDto;
import ru.skillbox.postservice.dto.PostRequest;
import ru.skillbox.postservice.entity.Post;
import ru.skillbox.postservice.exception.RecordNotFoundException;
import ru.skillbox.postservice.mappers.PostMapper;
import ru.skillbox.postservice.repository.PostRepository;
import ru.skillbox.postservice.service.PostService;
import ru.skillbox.postservice.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PostDefaultServiceTest {
    private final UserService userService = mock(UserService.class);
    private final PostMapper postMapper = mock(PostMapper.class);
    private final PostRepository postRepository = mock(PostRepository.class);
    private final PostService postService = new PostDefaultService(
            userService,
            postMapper,
            postRepository
    );

    @Test
    @DisplayName("Проверка вызова валидации пользователя при создании поста")
    public void whenCreate_thenValidateUser() {
        PostRequest request = new PostRequest();
        request.setUserId(UUID.randomUUID());

        postService.create(request);

        verify(userService, times(1)).validate(request.getUserId());
    }

    @Test
    @DisplayName("Проверка обновления несуществующего поста")
    public void whenUpdateNotExistedPost_thenException() {
        when(postRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, () -> postService.update(UUID.randomUUID(), new PostRequest()));
    }

    @Test
    @DisplayName("Проверка обновления поста")
    public void whenUpdate_thenSaveUpdatedPost() {
        Post post = new Post();
        UUID id = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        String description = "Description";
        String title = "Title";
        post.setId(id);
        post.setUserId(userId);
        post.setDescriptions(description);
        post.setTitle(title);

        PostRequest request = new PostRequest();
        request.setDescriptions("Updated description");
        request.setTitle("Updated title");

        when(postRepository.findById(any())).thenReturn(Optional.of(post));

        postService.update(id, request);

        assertEquals(request.getDescriptions(), post.getDescriptions());
        assertEquals(request.getTitle(), post.getTitle());
        assertEquals(id, post.getId());
        assertEquals(userId, post.getUserId());
    }

    @Test
    @DisplayName("Проверка удаления несуществующего поста")
    public void whenDeleteNotExistedPost_thenException() {
        when(postRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, () -> postService.delete(UUID.randomUUID()));
    }

    @Test
    @DisplayName("Проверка удаления поста")
    public void whenDelete_thenRepositoryDeleteCall() {
        when(postRepository.findById(any())).thenReturn(Optional.of(new Post()));

        postService.delete(UUID.randomUUID());

        verify(postRepository, times(1)).delete(any());
    }

    @Test
    @DisplayName("Проверка получения всех постов")
    public void whenGetAll_thenList() {
        when(postRepository.findAll()).thenReturn(List.of(new Post(), new Post()));
        when(postMapper.toPostDtos(any(List.class))).thenReturn(List.of(
                PostDto.builder().build(), PostDto.builder().build()
        ));

        assertEquals(2, postService.getAll().size());
    }

    @Test
    @DisplayName("Проверка поиска несуществующего поста")
    public void whenGetNotExistedPost_thenException() {
        when(postRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, () -> postService.getOneById(UUID.randomUUID()));
    }

    @Test
    @DisplayName("Проверка поиска поста")
    public void whenGetOneById_thenPostDto() {
        when(postRepository.findById(any())).thenReturn(Optional.of(new Post()));
        when(postMapper.toPostDto(any())).thenReturn(PostDto.builder().build());

        assertNotNull(postService.getOneById(UUID.randomUUID()));
    }
}