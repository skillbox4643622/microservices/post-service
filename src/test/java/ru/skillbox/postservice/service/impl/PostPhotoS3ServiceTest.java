package ru.skillbox.postservice.service.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.postservice.dto.PhotoDto;
import ru.skillbox.postservice.dto.PostDto;
import ru.skillbox.postservice.entity.PostPhoto;
import ru.skillbox.postservice.exception.RecordNotFoundException;
import ru.skillbox.postservice.mappers.PostPhotoMapper;
import ru.skillbox.postservice.properties.S3Properties;
import ru.skillbox.postservice.repository.PostPhotoRepository;
import ru.skillbox.postservice.repository.S3Repository;
import ru.skillbox.postservice.service.PostPhotoService;
import ru.skillbox.postservice.service.PostService;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PostPhotoS3ServiceTest {
    private final PostService postService = mock(PostService.class);
    private final PostPhotoMapper photoMapper = mock(PostPhotoMapper.class);
    private final PostPhotoRepository photoRepository = mock(PostPhotoRepository.class);
    private final S3Repository s3Repository = mock(S3Repository.class);
    private final S3Properties s3Properties = mock(S3Properties.class);
    private final PostPhotoService postPhotoService = new PostPhotoS3Service(
            postService,
            photoMapper,
            photoRepository,
            s3Repository,
            s3Properties
    );

    @Test
    @DisplayName("Проверка возврата фотографий по идентификатору поста")
    public void whenAllByPostId_thenGetListPhotoDto() {
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(List.of(
                PhotoDto.builder().build(),
                PhotoDto.builder().build()
        )).build());

        assertEquals(2, postPhotoService.getAllByPostId(UUID.randomUUID()).size());
    }

    @Test
    @DisplayName("Проверка возврата фотографий по идентификатору поста и фотографии")
    public void whenGetByPostIdAndPhotoIdExisted_thenGetPhotoDto() {
        UUID neededId = UUID.randomUUID();
        UUID notNeededId = UUID.randomUUID();
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(List.of(
                PhotoDto.builder().id(notNeededId).build(),
                PhotoDto.builder().id(neededId).build()
        )).build());

        assertEquals(neededId, postPhotoService.getByPostIdAndPhotoId(UUID.randomUUID(), neededId).getId());
    }

    @Test
    @DisplayName("Проверка исключения при поиске по идентификатору поста и фотографии")
    public void whenGetByPostIdAndPhotoIdNotExisted_thenException() {
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(List.of(
                PhotoDto.builder().id(UUID.randomUUID()).build(),
                PhotoDto.builder().id(UUID.randomUUID()).build()
        )).build());

        assertThrows(
                RecordNotFoundException.class,
                () -> postPhotoService.getByPostIdAndPhotoId(UUID.randomUUID(), UUID.randomUUID())
        );
    }

    @Test
    @DisplayName("Проверка сохранения множества фотографий при пустом идентификаторе поста")
    public void whenSaveAllWithNullPostId_thenEmptyList() {
        MultipartFile multipartFile = mock(MultipartFile.class);
        assertEquals(0, postPhotoService.saveAll(null, List.of(multipartFile)).size());
    }

    @Test
    @DisplayName("Проверка сохранения множества фотографий при отсутствии фотографий")
    public void whenSaveAllWithNullPhotoList_thenEmptyList() {
        assertEquals(0, postPhotoService.saveAll(UUID.randomUUID(), null).size());
    }

    @Test
    @DisplayName("Проверка сохранения множества фотографий")
    public void whenSaveAll_thenList() {
        MultipartFile multipartFile1 = mock(MultipartFile.class);
        MultipartFile multipartFile2 = mock(MultipartFile.class);
        MultipartFile multipartFile3 = mock(MultipartFile.class);

        when(photoRepository.save(any())).thenReturn(new PostPhoto());
        when(photoMapper.toPostPhotoDto(any(PostPhoto.class))).thenReturn(PhotoDto.builder().build());

        assertEquals(
                3,
                postPhotoService
                        .saveAll(UUID.randomUUID(), List.of(multipartFile1, multipartFile2, multipartFile3))
                        .size()
        );
    }

    @Test
    @DisplayName("Проверка обновления несуществующей фотографии")
    public void whenUpdateNotExistedPhoto_thenException() {
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(Collections.emptyList()).build());
        assertThrows(
                RecordNotFoundException.class,
                () -> postPhotoService.update(UUID.randomUUID(), UUID.randomUUID(), mock(MultipartFile.class))
        );
    }

    @Test
    @DisplayName("Проверка вызова репозиториев при обновлении фотографии")
    public void whenUpdatePhoto_thenS3RepositoryAndPhotoRepositoryCalled() {
        UUID photoId = UUID.randomUUID();
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(List.of(
                PhotoDto.builder().id(photoId).build())
        ).build());

        postPhotoService.update(UUID.randomUUID(), photoId, mock(MultipartFile.class));

        verify(s3Repository, times(1)).put(any(), any(), any(), any());
        verify(photoRepository, times(1)).save(any());
    }

    @Test
    @DisplayName("Проверка удаления несуществующей фотографии")
    public void whenDeleteNotExistedPhoto_thenException() {
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(Collections.emptyList()).build());
        assertThrows(
                RecordNotFoundException.class,
                () -> postPhotoService.delete(UUID.randomUUID(), UUID.randomUUID())
        );
    }

    @Test
    @DisplayName("Проверка вызова репозиториев при удалении фотографии")
    public void whenDeletePhoto_thenS3RepositoryAndPhotoRepositoryCalled() {
        UUID photoId = UUID.randomUUID();
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(List.of(
                PhotoDto.builder().id(photoId).build())
        ).build());

        postPhotoService.delete(UUID.randomUUID(), photoId);

        verify(s3Repository, times(1)).delete(any(), any());
        verify(photoRepository, times(1)).deleteById(any());
    }

    @Test
    @DisplayName("Проверка вызова репозиториев при удалении всех фотографий поста")
    public void whenDeletePhotos_thenS3RepositoryAndPhotoRepositoryCalled() {
        when(postService.getOneById(any())).thenReturn(PostDto.builder().photos(List.of(
                PhotoDto.builder().build(),
                PhotoDto.builder().build()
        )).build());

        postPhotoService.delete(UUID.randomUUID());

        verify(s3Repository, times(2)).delete(any(), any());
        verify(photoRepository, times(2)).deleteById(any());
    }
}