package ru.skillbox.postservice.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.skillbox.postservice.properties.S3Properties;
import ru.skillbox.postservice.repository.S3Repository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class BucketServiceTest {
    private final S3Repository s3Repository = mock(S3Repository.class);
    private final S3Properties s3Properties = mock(S3Properties.class);
    @Test
    @DisplayName("Проверка, что при наличии бакета новый не создается")
    public void whenBucketExist_thenNotCreateBucket() {
        when(s3Repository.doesBucketExist(any())).thenReturn(true);

        new BucketService(s3Repository, s3Properties);

        verify(s3Repository, times(0)).createBucket(any());
    }

    @Test
    @DisplayName("Проверка, что при отсутствии бакета новый создается")
    public void whenBucketNotExist_thenCreateBucket() {
        when(s3Repository.doesBucketExist(any())).thenReturn(false);

        new BucketService(s3Repository, s3Properties);

        verify(s3Repository, times(1)).createBucket(any());
    }
}