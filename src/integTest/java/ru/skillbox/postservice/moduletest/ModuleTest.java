package ru.skillbox.postservice.moduletest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.skillbox.postservice.AbstractModuleTest;
import ru.skillbox.postservice.dto.PhotoDto;
import ru.skillbox.postservice.dto.PostDto;

import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class ModuleTest extends AbstractModuleTest {

    private PostDto createPost() throws Exception {
        wireMockExtension.stubFor(get("/user/3fa85f64-5717-4562-b3fc-2c963f66afa6").willReturn(ok()));
        MvcResult result = mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post("/post")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asBytes("json/create-post-valid.json"))
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        return getObjectFromResponse(result, objectMapper, PostDto.class);
    }

    private PhotoDto createPhoto(PostDto postDto) throws Exception {
        MockMultipartFile file = new MockMultipartFile(
                "files",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Test".getBytes()
        );
        MvcResult result = mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart(
                                "/post/" + postDto.getId() + "/photo"
                        ).file(file).content(MediaType.MULTIPART_FORM_DATA_VALUE)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        return getObjectFromResponse(result, objectMapper, PhotoDto[].class)[0];
    }

    private void deletePost(UUID id) throws Exception {
        mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete(
                                "/post/" + id
                        ).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk());
    }
    @Test
    @DisplayName("Создание поста")
    public void whenCreatePost_thenReturnPostDto() throws Exception {
        PostDto postDto = createPost();

        Assertions.assertNotNull(postDto.getId());
        Assertions.assertEquals("3fa85f64-5717-4562-b3fc-2c963f66afa6", postDto.getUserId().toString());
        Assertions.assertEquals("title", postDto.getTitle());
        Assertions.assertEquals("description", postDto.getDescriptions());

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Запрос поста")
    public void whenGetPost_thenReturnPostDto() throws Exception {
        PostDto postDto = createPost();
        MvcResult result = mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get(
                                "/post/" + postDto.getId()
                                ).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        PostDto postResponse = getObjectFromResponse(result, objectMapper, PostDto.class);

        Assertions.assertEquals(postDto.getId(), postResponse.getId());
        Assertions.assertEquals(postDto.getUserId(), postResponse.getUserId());
        Assertions.assertEquals(postDto.getTitle(), postResponse.getTitle());
        Assertions.assertEquals(postDto.getDescriptions(), postResponse.getDescriptions());

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Запрос постов")
    public void whenGetPost_thenReturnListPostDto() throws Exception {
        PostDto postDto = createPost();

        MvcResult result = mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/post")
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        PostDto[] postDtos = getObjectFromResponse(result, objectMapper, PostDto[].class);

        Assertions.assertEquals(1, postDtos.length);
        Assertions.assertNotNull(postDtos[0].getId());
        Assertions.assertEquals(postDto.getUserId(), postDtos[0].getUserId());
        Assertions.assertEquals(postDto.getTitle(), postDtos[0].getTitle());
        Assertions.assertEquals(postDto.getDescriptions(), postDtos[0].getDescriptions());

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Создание поста несуществующим пользователем")
    public void whenCreatePostWithNotExistedUser_thenBadRequest() throws Exception {
        wireMockExtension.stubFor(get("/user/3fa85f64-5717-4562-b3fc-2c963f66afa6").willReturn(noContent()));
        mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post("/post")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asBytes("json/create-post-valid.json"))
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @DisplayName("Обновление поста")
    public void whenUpdatePost_thenPostDto() throws Exception {
        PostDto postDto = createPost();
        MvcResult result = mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put(
                                "/post/" + postDto.getId()
                                ).contentType(MediaType.APPLICATION_JSON)
                                .content(asBytes("json/update-post-valid.json"))
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        PostDto post = getObjectFromResponse(result, objectMapper, PostDto.class);

        Assertions.assertEquals(postDto.getId(), post.getId());
        Assertions.assertNotEquals(postDto.getDescriptions(), post.getDescriptions());
        Assertions.assertNotEquals(postDto.getTitle(), post.getTitle());

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Создание фотографии")
    public void whenCreatePhoto_thenReturnPhotoDto() throws Exception {
        PostDto postDto = createPost();
        PhotoDto photoDto = createPhoto(postDto);

        Assertions.assertEquals("hello.txt", photoDto.getName());

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Обновление фотографии")
    public void whenUpdatePhoto_thenReturnPhotoDto() throws Exception {
        PostDto postDto = createPost();
        PhotoDto photoDto = createPhoto(postDto);

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "hello2.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Test2".getBytes()
        );

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/post/" + postDto.getId() + "/photo/" + photoDto.getId());

        builder.with(request -> {
            request.setMethod("PUT");
            return request;
        });

        MvcResult result = mockMvc
                .perform(
                        builder.file(file).content(MediaType.MULTIPART_FORM_DATA_VALUE)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        photoDto = getObjectFromResponse(result, objectMapper, PhotoDto.class);

        Assertions.assertEquals("hello2.txt", photoDto.getName());

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Получить фотографии поста")
    public void whenGetPostPhotos_thenReturnPostDtoList() throws Exception {
        PostDto postDto = createPost();
        PhotoDto photoDto = createPhoto(postDto);
        MvcResult result = mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get(
                                "/post/" + postDto.getId() + "/photo"
                        ).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        Assertions.assertEquals(1, getObjectFromResponse(result, objectMapper, PhotoDto[].class).length);

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Получить фотографию поста")
    public void whenGetPostPhoto_thenReturnPostDto() throws Exception {
        PostDto postDto = createPost();
        PhotoDto photoDto = createPhoto(postDto);
        MvcResult result = mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get(
                                "/post/" + postDto.getId() + "/photo/" + photoDto.getId()
                        ).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andReturn();

        Assertions.assertEquals(
                photoDto.getName(),
                getObjectFromResponse(result, objectMapper, PhotoDto.class).getName()
        );

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Удалить фотографию поста")
    public void whenDeletePostPhoto_thenReturnOk() throws Exception {
        PostDto postDto = createPost();
        PhotoDto photoDto = createPhoto(postDto);
        mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete(
                                "/post/" + postDto.getId() + "/photo/" + photoDto.getId()
                        ).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk());

        mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get(
                                "/post/" + postDto.getId() + "/photo/" + photoDto.getId()
                        ).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isNoContent());

        deletePost(postDto.getId());
    }

    @Test
    @DisplayName("Удалить пост")
    public void whenDeletePost_thenReturnOk() throws Exception {
        PostDto postDto = createPost();
        PhotoDto photoDto = createPhoto(postDto);

        deletePost(postDto.getId());

        mockMvc
                .perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get(
                                "/post/" + postDto.getId()
                        ).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isNoContent());
    }
}
