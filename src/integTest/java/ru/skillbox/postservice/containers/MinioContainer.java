package ru.skillbox.postservice.containers;

import org.testcontainers.containers.GenericContainer;

import java.util.Map;

public class MinioContainer extends GenericContainer<MinioContainer> {
    public static final String IMAGE_NAME = "bitnami/minio:2023.6.9";
    public static final String ACCESS_KEY = "it7qQfcBeJsndIPUd3LU";
    public static final String SECRET_KEY = "mdUpjgk96mWm98bg5jTXsXa17zWV61UUkV3NJ0tC";
    public static final Map<String, String> ENV = Map.of(
            "BITNAMI_DEBUG", "true",
            "MINIO_ROOT_USER", ACCESS_KEY,
            "MINIO_ROOT_PASSWORD", SECRET_KEY
    );
    public static final int EXPOSED_PORT = 9000;



    public MinioContainer() {
        super(IMAGE_NAME);
        addExposedPort(EXPOSED_PORT);
        this.withEnv(ENV);
    }
}
