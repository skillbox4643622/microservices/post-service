package ru.skillbox.postservice.containers;

import org.testcontainers.containers.PostgreSQLContainer;

public class PostgreContainer extends PostgreSQLContainer<PostgreContainer> {
    private static final String POSTGRES_IMAGE_NAME = "postgres:12";
    private static final String POSTGRES_DB = "integtest_postgres";
    private static final String POSTGRES_USER = "test";
    private static final String POSTGRES_PASSWORD = "test";

    public PostgreContainer() {
        super(POSTGRES_IMAGE_NAME);
        this.withDatabaseName(POSTGRES_DB)
                .withUsername(POSTGRES_USER)
                .withPassword(POSTGRES_PASSWORD)
                .withInitScript("postgre-init.sql");
    }
}
