package ru.skillbox.postservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import ru.skillbox.postservice.containers.MinioContainer;
import ru.skillbox.postservice.containers.PostgreContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.FileCopyUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@SpringBootTest
@DirtiesContext
@AutoConfigureMockMvc
@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AbstractModuleTest {
    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Container
    protected static final PostgreSQLContainer<PostgreContainer> postgresContainer = new PostgreContainer();
    @Container
    protected static final MinioContainer miniContainer = new MinioContainer();
    @RegisterExtension
    protected static final WireMockExtension wireMockExtension = WireMockExtension
            .newInstance()
            .options(wireMockConfig().dynamicPort())
            .build();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);

        registry.add("spring.liquibase.url", postgresContainer::getJdbcUrl);
        registry.add("spring.liquibase.user", postgresContainer::getUsername);
        registry.add("spring.liquibase.default-schema", postgresContainer::getUsername);
        registry.add("spring.liquibase.liquibase-schema", postgresContainer::getUsername);
        registry.add("spring.liquibase.password", postgresContainer::getPassword);

        registry.add("user-service.host", wireMockExtension::baseUrl);

        registry.add("s3.endpoint", () -> "http://localhost:" + miniContainer.getMappedPort(MinioContainer.EXPOSED_PORT));
        registry.add("s3.access-key", () -> MinioContainer.ACCESS_KEY);
        registry.add("s3.secret-key", () -> MinioContainer.SECRET_KEY);
    }

    protected byte[] asBytes(String resourceName) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(resourceName);
        try {
            return FileCopyUtils.copyToByteArray(resource.getFile());
        } catch (IOException e) {
            throw new RuntimeException("asBytes error", e);
        }
    }

    protected byte[] asBytes(String resourceName, Map<String, String> replaceProperties) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(resourceName);
        try {
            StringBuilder content = new StringBuilder(resource.getContentAsString(StandardCharsets.UTF_8));
            replaceProperties.forEach((key, value) -> {
                content.replace(0, content.length(), content.toString().replaceAll(key, value));
            });
            return content.toString().getBytes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected <T> T getObjectFromResponse(
            MvcResult mvcResult,
            ObjectMapper objectMapper,
            Class<T> clazz
    ) throws JsonProcessingException, UnsupportedEncodingException {
        String json = mvcResult.getResponse().getContentAsString();
        return asObject(json, objectMapper, clazz);
    }

    protected <T> T asObject(String json, ObjectMapper objectMapper, Class<T> clazz) throws JsonProcessingException {
        return objectMapper.readValue(json, clazz);
    }
}
