package ru.skillbox.postservice.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "post_photo")
@Getter
@Setter
public class PostPhoto {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    @Column(name = "post_id")
    private UUID postId;
    private String name;
}
