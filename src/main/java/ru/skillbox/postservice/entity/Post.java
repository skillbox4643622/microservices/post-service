package ru.skillbox.postservice.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "post")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "user_id")
    private UUID userId;
    private String title;
    private String descriptions;

    @OneToMany(mappedBy = "postId")
    private Set<PostPhoto> postPhotos = new HashSet<>();
}
