package ru.skillbox.postservice.service;

import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.postservice.dto.PhotoDto;

import java.util.List;
import java.util.UUID;

public interface PostPhotoService {
    List<PhotoDto> getAllByPostId(UUID postId);

    PhotoDto getByPostIdAndPhotoId(UUID postId, UUID photoId);

    List<PhotoDto> saveAll(UUID postId, List<MultipartFile> files);

    PhotoDto update(UUID postId, UUID photoId, MultipartFile file);

    void delete(UUID postId);

    void delete(UUID postId, UUID photoId);
}
