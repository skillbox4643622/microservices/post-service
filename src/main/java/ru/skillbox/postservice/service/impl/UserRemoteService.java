package ru.skillbox.postservice.service.impl;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.skillbox.postservice.exception.UserNotFoundException;
import ru.skillbox.postservice.properties.UserServiceProperties;
import ru.skillbox.postservice.service.UserService;

import java.net.URI;
import java.util.UUID;

@Service
public class UserRemoteService implements UserService {
    private final UserServiceProperties properties;
    private RestTemplate restTemplate;

    public UserRemoteService(UserServiceProperties properties, RestTemplateBuilder builder) {
        this.properties = properties;
        this.restTemplate = builder.build();
    }

    @Override
    public void validate(UUID id) {
        RequestEntity<String> request = new RequestEntity<>(
                HttpMethod.GET,
                URI.create(properties.getHost() + "/" + properties.getUserEndpoint() + "/" + id.toString())
        );
        ResponseEntity<String> response = restTemplate.exchange(request, String.class);

        if (response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
            throw new UserNotFoundException(id);
        }
    }
}
