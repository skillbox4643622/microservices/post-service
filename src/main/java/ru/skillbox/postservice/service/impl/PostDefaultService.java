package ru.skillbox.postservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.skillbox.postservice.dto.PostDto;
import ru.skillbox.postservice.dto.PostRequest;
import ru.skillbox.postservice.entity.Post;
import ru.skillbox.postservice.exception.RecordNotFoundException;
import ru.skillbox.postservice.mappers.PostMapper;
import ru.skillbox.postservice.repository.PostRepository;
import ru.skillbox.postservice.service.PostService;
import ru.skillbox.postservice.service.UserService;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PostDefaultService implements PostService {
    private final UserService userService;
    private final PostMapper postMapper;
    private final PostRepository postRepository;

    @Override
    public PostDto create(PostRequest request) {
        userService.validate(request.getUserId());
        return postMapper.toPostDto(postRepository.save(postMapper.toPost(request)));
    }

    @Override
    public PostDto update(UUID id, PostRequest request) {
        Post post = findById(id);
        post.setDescriptions(request.getDescriptions());
        post.setTitle(request.getTitle());
        return postMapper.toPostDto(postRepository.save(post));
    }

    @Override
    public void delete(UUID id) {
        Post post = findById(id);
        postRepository.delete(post);
    }

    @Override
    public List<PostDto> getAll() {
        return postMapper.toPostDtos(postRepository.findAll());
    }

    @Override
    public PostDto getOneById(UUID id) {
        return postMapper.toPostDto(findById(id));
    }

    private Post findById(UUID id) {
        return postRepository.findById(id).orElseThrow(
                () -> new RecordNotFoundException(String.format("Post with id %s not found", id))
        );
    }
}
