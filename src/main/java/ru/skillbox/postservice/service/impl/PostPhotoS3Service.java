package ru.skillbox.postservice.service.impl;

import com.amazonaws.services.s3.model.ObjectMetadata;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.postservice.dto.PhotoDto;
import ru.skillbox.postservice.entity.PostPhoto;
import ru.skillbox.postservice.exception.RecordNotFoundException;
import ru.skillbox.postservice.mappers.PostPhotoMapper;
import ru.skillbox.postservice.properties.S3Properties;
import ru.skillbox.postservice.repository.PostPhotoRepository;
import ru.skillbox.postservice.repository.S3Repository;
import ru.skillbox.postservice.service.PostPhotoService;
import ru.skillbox.postservice.service.PostService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PostPhotoS3Service implements PostPhotoService {
    private final PostService postService;
    private final PostPhotoMapper photoMapper;
    private final PostPhotoRepository photoRepository;
    private final S3Repository repository;
    private final S3Properties properties;

    @Override
    public List<PhotoDto> getAllByPostId(UUID postId) {
        return postService.getOneById(postId).getPhotos();
    }

    @Override
    public PhotoDto getByPostIdAndPhotoId(UUID postId, UUID photoId) {
        return postService.getOneById(postId).getPhotos().stream()
                .filter(p -> p.getId().equals(photoId))
                .findFirst()
                .orElseThrow(() -> new RecordNotFoundException(String.format("Photo with id %s not found", photoId)));
    }

    @Override
    public List<PhotoDto> saveAll(UUID postId, List<MultipartFile> files) {
        List<PhotoDto> photoDtos = new ArrayList<>();
        if (postId == null || files == null) {
            return photoDtos;
        }

        files.forEach(file -> {
            photoDtos.add(save(postId, file));
        });

        return photoDtos;
    }

    private PhotoDto save(UUID postId, MultipartFile file) {
        try {
            repository.put(
                    properties.getPostPhotoBucket(),
                    file.getOriginalFilename(),
                    file.getInputStream(),
                    new ObjectMetadata()
            );
            PostPhoto postPhoto = new PostPhoto();
            postPhoto.setName(file.getOriginalFilename());
            postPhoto.setPostId(postId);
            return photoMapper.toPostPhotoDto(photoRepository.save(postPhoto));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PhotoDto update(UUID postId, UUID photoId, MultipartFile file) {
        delete(postId, photoId);
        return save(postId, file);
    }

    @Override
    public void delete(UUID postId) {
        List<PhotoDto> photoDtos = getAllByPostId(postId);
        photoDtos.forEach(photo -> {
            repository.delete(properties.getPostPhotoBucket(), photo.getName());
            photoRepository.deleteById(photo.getId());
        });
    }

    @Override
    public void delete(UUID postId, UUID photoId) {
        PhotoDto photoDto = getByPostIdAndPhotoId(postId, photoId);
        repository.delete(properties.getPostPhotoBucket(), photoDto.getName());
        photoRepository.deleteById(photoDto.getId());
    }
}
