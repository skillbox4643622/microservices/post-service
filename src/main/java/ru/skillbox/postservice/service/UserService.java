package ru.skillbox.postservice.service;

import java.util.UUID;

public interface UserService {
    void validate(UUID id);
}
