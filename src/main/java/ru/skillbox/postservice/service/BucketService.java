package ru.skillbox.postservice.service;

import com.amazonaws.services.s3.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.skillbox.postservice.properties.S3Properties;
import ru.skillbox.postservice.repository.S3Repository;

@Service
@Slf4j
public class BucketService {
    private final S3Repository s3Repository;
    private final S3Properties s3Properties;

    public BucketService(S3Repository s3Repository, S3Properties s3Properties) {
        this.s3Repository = s3Repository;
        this.s3Properties = s3Properties;

        checkAndCreateRequiredBuckets();
    }

    private void checkAndCreateRequiredBuckets() {
        log.info("S3 properties: {}", s3Properties);
        if (s3Repository.doesBucketExist(s3Properties.getPostPhotoBucket())) {
            log.info("Bucket {} already exist", s3Properties.getPostPhotoBucket());
            return;
        }
        log.info("Bucket {} is not exist and will be created", s3Properties.getPostPhotoBucket());
        CreateBucketRequest request = new CreateBucketRequest(s3Properties.getPostPhotoBucket());
        AccessControlList acl = new AccessControlList();
        acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
        request.setAccessControlList(acl);

        s3Repository.createBucket(request);
    }
}
