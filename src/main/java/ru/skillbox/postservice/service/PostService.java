package ru.skillbox.postservice.service;

import ru.skillbox.postservice.dto.PostDto;
import ru.skillbox.postservice.dto.PostRequest;

import java.util.List;
import java.util.UUID;

public interface PostService {
    PostDto create(PostRequest request);
    PostDto update(UUID id, PostRequest request);
    void delete(UUID id);
    List<PostDto> getAll();
    PostDto getOneById(UUID id);
}
