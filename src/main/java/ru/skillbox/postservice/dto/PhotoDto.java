package ru.skillbox.postservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
public class PhotoDto {
    private UUID id;
    private String link;
    private String name;
}
