package ru.skillbox.postservice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class PostRequest {
    @NotNull
    private UUID userId;
    @NotBlank
    private String title;
    private String descriptions;
}
