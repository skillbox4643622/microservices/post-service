package ru.skillbox.postservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
public class PostDto extends PostRequest {
    private UUID id;
    private UUID userId;
    private String title;
    private String descriptions;
    private List<PhotoDto> photos;
}
