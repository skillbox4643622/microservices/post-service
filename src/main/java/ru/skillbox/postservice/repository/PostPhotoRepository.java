package ru.skillbox.postservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.skillbox.postservice.entity.PostPhoto;

import java.util.UUID;

@Repository
public interface PostPhotoRepository extends JpaRepository<PostPhoto, UUID> {
}
