package ru.skillbox.postservice.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.InputStream;

@Component
@RequiredArgsConstructor
public class S3Repository {
    private final AmazonS3 client;

    public boolean doesBucketExist(String bucketName) {
        return client.doesBucketExistV2(bucketName);
    }

    public Bucket createBucket(CreateBucketRequest request) {
        return client.createBucket(request);
    }

    public PutObjectResult put(String bucket, String key, InputStream inputStream, ObjectMetadata metadata) {
        return client.putObject(bucket, key, inputStream, metadata);
    }

    public ListObjectsV2Result findAllByBucketAndPrefix(String bucket, String prefix) {
        return client.listObjectsV2(bucket, prefix);
    }

    public void delete(String bucket, String key) {
        client.deleteObject(bucket, key);
    }
}
