package ru.skillbox.postservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.postservice.dto.PhotoDto;
import ru.skillbox.postservice.dto.PostDto;
import ru.skillbox.postservice.dto.PostRequest;
import ru.skillbox.postservice.service.PostPhotoService;
import ru.skillbox.postservice.service.PostService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/post")
@RequiredArgsConstructor
@Tag(name = "Post")
public class PostController {
    private final PostService postService;
    private final PostPhotoService photoService;

    @GetMapping
    @Operation(summary = "Получить все посты")
    @ApiResponse(responseCode = "200", description = "OK")
    public ResponseEntity<List<PostDto>> getAllPosts() {
        return ResponseEntity.ok(postService.getAll());
    }

    @PostMapping
    @Operation(summary = "Создать пост")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Некорректное тело запроса")
    })
    public ResponseEntity<PostDto> createPost(@RequestBody @Valid PostRequest request) {
        return ResponseEntity.ok(postService.create(request));
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "Получить пост")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост не найден")
    })
    public ResponseEntity<PostDto> getPostById(@PathVariable @NotNull @Valid UUID id) {
        return ResponseEntity.ok(postService.getOneById(id));
    }


    @PutMapping(value = "/{id}")
    @Operation(summary = "Обновить пост")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректное тело запроса")
    })
    public ResponseEntity<PostDto> updatePostById(
            @PathVariable @NotNull @Valid UUID id,
            @RequestBody @Valid PostRequest request
    ) {
        return ResponseEntity.ok(postService.update(id, request));
    }

    @DeleteMapping(value = "/{id}")
    @Transactional
    @Operation(summary = "Удалить пост и его фотографии")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост не найден")
    })
    public void deletePostById(@PathVariable @NotNull @Valid UUID id) {
        photoService.delete(id);
        postService.delete(id);
    }

    private <T> String getT(T t) {
        return t.toString();
    }

    @GetMapping(value = "/{id}/photo")
    @Operation(summary = "Получить фотографии поста")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост не найден")
    })
    public ResponseEntity<List<PhotoDto>> getPhotosByPostId(@PathVariable @NotNull @Valid UUID id) {
        return ResponseEntity.ok(photoService.getAllByPostId(id));
    }

    @PostMapping(value = "/{id}/photo")
    @Operation(summary = "Создать фотографию поста")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректное тело запроса")
    })
    public ResponseEntity<List<PhotoDto>> createPostPhoto(
            @PathVariable @NotNull @Valid UUID id,
            @RequestBody @NotNull @Valid List<MultipartFile> files
    ) {
        return ResponseEntity.ok(photoService.saveAll(id, files));
    }

    @GetMapping(value = "/{postId}/photo/{photoId}")
    @Operation(summary = "Получить фото по идентификатору поста и фотографии")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост или фотография не найдены")
    })
    public ResponseEntity<PhotoDto> getPhotoByPostIdAndPhotoId(
            @PathVariable @NotNull @Valid UUID postId,
            @PathVariable @NotNull @Valid UUID photoId
    ) {
        return ResponseEntity.ok(photoService.getByPostIdAndPhotoId(postId, photoId));
    }

    @PutMapping(value = "/{postId}/photo/{photoId}")
    @Operation(summary = "Обновить фотографию по идентификатору поста и фотографии")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост или фотография не найдены"),
            @ApiResponse(responseCode = "400", description = "Некорректное тело запроса")
    })
    public ResponseEntity<PhotoDto> updatePhotoByPostIdAndPhotoId(
            @PathVariable @NotNull @Valid UUID postId,
            @PathVariable @NotNull @Valid UUID photoId,
            @RequestBody @Valid MultipartFile file
    ) {
        return ResponseEntity.ok(photoService.update(postId, photoId, file));
    }

    @DeleteMapping(value = "/{postId}/photo/{photoId}")
    @Operation(summary = "Удалить фотографию поста")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "Пост или фотография не найдены")
    })
    public void deletePhotoByPostIdAndPhotoId(
            @PathVariable @NotNull @Valid UUID postId,
            @PathVariable @NotNull @Valid UUID photoId
    ) {
        photoService.delete(postId, photoId);
    }
}
