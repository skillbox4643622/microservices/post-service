package ru.skillbox.postservice.mappers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.skillbox.postservice.dto.PostDto;
import ru.skillbox.postservice.dto.PostRequest;
import ru.skillbox.postservice.entity.Post;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostMapper {
    private final PostPhotoMapper photoMapper;

    public Post toPost(PostRequest request) {
        if (request == null) {
            return null;
        }

        Post post = new Post();
        post.setTitle(request.getTitle());
        post.setDescriptions(request.getDescriptions());
        post.setUserId(request.getUserId());
        return post;
    }

    public PostDto toPostDto(Post post) {
        if (post == null) {
            return null;
        }

        return PostDto.builder()
                .id(post.getId())
                .title(post.getTitle())
                .descriptions(post.getDescriptions())
                .userId(post.getUserId())
                .photos(photoMapper.toPostPhotoDto(post.getPostPhotos()))
                .build();
    }

    public List<PostDto> toPostDtos(List<Post> posts) {
        if (posts == null) {
            return null;
        }

        List<PostDto> postDtos = new ArrayList<>();
        posts.forEach(post -> postDtos.add(toPostDto(post)));
        return postDtos;
    }
}
