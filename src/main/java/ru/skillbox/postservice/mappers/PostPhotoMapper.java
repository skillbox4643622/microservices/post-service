package ru.skillbox.postservice.mappers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.skillbox.postservice.dto.PhotoDto;
import ru.skillbox.postservice.entity.PostPhoto;
import ru.skillbox.postservice.properties.S3Properties;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostPhotoMapper {
    public final static String PHOTO_LINK_DELIMETER = "/";

    private final S3Properties properties;

    public PhotoDto toPostPhotoDto(PostPhoto postPhoto) {
        if (postPhoto == null) {
            return null;
        }

        return PhotoDto.builder()
                .id(postPhoto.getId())
                .name(postPhoto.getName())
                .link(properties.getS3Url() + PHOTO_LINK_DELIMETER + properties.getPostPhotoBucket()
                        + PHOTO_LINK_DELIMETER + postPhoto.getName())
                .build();
    }

    public List<PhotoDto> toPostPhotoDto(Iterable<PostPhoto> postPhotos) {
        if (postPhotos == null) {
            return null;
        }

        List<PhotoDto> photos = new ArrayList<>();
        postPhotos.forEach(photo -> photos.add(toPostPhotoDto(photo)));
        return photos;
    }
}
